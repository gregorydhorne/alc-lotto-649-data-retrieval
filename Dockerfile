# Data Retrieval and Extraction Tool for Atlantic Lottery Corporation's Lotto 649
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Copyright (c) 2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

FROM alpine:3.8

MAINTAINER Gregory D. Horne < greg at gregoryhorne dot ca >

# Install command-line  web page retrieval and extraction support tools.

RUN apk add --no-cache --update --upgrade \
    ca-certificates \
    jq zip

# Create a non-privleged user account.

RUN adduser -u 1000 -D user

USER user

VOLUME /home/user/project
WORKDIR /home/user/project

ENTRYPOINT ["sh"]
