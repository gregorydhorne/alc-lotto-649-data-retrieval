#!/usr/bin/env sh

# Lottery Data Retrieval and Extraction for Atlantic Lottery Corporation
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Original source:
#   https://gitlab.com/gregorydhorne/alc-lotto-649-data-retrieval
# Copyright (c) 2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

# Configuration Settings
BROWSER="wget -O ./data/page.html"
PROTOCOL="https"
DOMAIN="www.alc.ca"
ASSET="/content/alc/en/our-games/lotto/lotto-6-49.html"

# Retrieve lottery results for the draw date.
retrieve_lottery_results()
{
  # If the results for the draw date have already been retrieved, do not fetch
  # them again.
  if [[ -e ./data/lotto649.csv ]] && [[ ! -z `grep "${draw_date}" ./data/lotto649.csv` ]]
  then
    echo "Lottery results for ${draw_date} were previously retrieved"
    grep ${draw_date} ./data/lotto649.csv
    exit 1
  fi

  # Launch the web browser and retrieve the Lotto 649 winning numbers for the
  # specified draw date. The date is not validated to ensure it is an actual
  # draw date; if the date is incorrect, the lottery results returned are for
  # the draw date prior to the calendar date.
  QUERY="?date=${draw_date}"
  ${BROWSER} ${PROTOCOL}://${DOMAIN}${ASSET}${QUERY}

  # Check the web page was saved to a file named page.html.
  if [[ ! -e ./data/page.html ]]
  then
    echo "Save the web page to a file named page.html"
    exit 1
  fi
}

# Extract the gameId and gameData elements from the JSON data structure within
# the webpage. Create a well-formed JSON data structure.
extract_lottery_details()
{
  awk '
    /ALC.components.GameDetailComponent/ { flag = 1; next; }
    /jQuery/ { flag = 0; } flag
  ' ./data/page.html \
  | awk '
      BEGIN { print("["); }
      /gameId/ {
        sub("gameId", "\"gameId\"", $0);
        printf("{%s", $0);
      }
      /gameData/ {
        sub("gameData", "\"gameData\"", $0);
        sub(/,$/, "},", $0);
        printf("%s\n", $0);
      }
      END { printf("{}\n]"); }
  ' > ./data/page.json

  # Extract the winning numbers and the bonus number.
  numbers=$(jq --raw-output '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.winning_numbers | @csv' ./data/page.json |  sed 's/\"//g')
  bonus_number=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.bonus_number' ./data/page.json | sed 's/\"//g')

  # Extract the jackpot amount.
  jackpot=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.prize_payouts | .[] | select(.type=="Lotto649_6of6") | .prize_value' ./data/page.json)
  jackpot=$(printf "%0.2f" ${jackpot})
}

# Display the lottery results on the console.
write_details_to_console()
{
  printf "Lottery results for ${draw_date}: "
  printf "${numbers},${bonus_number},${jackpot}\n"
}

# Save the lottery results to a file named lotto649.csv.
write_details_to_file()
{
  printf "${draw_date},${numbers},${bonus_number},${jackpot}\n" >> ./data/lotto649.csv
}

# Clean-up any intermediary files.
clean_up()
{
  rm -f ./data/page.html ./data/page.json
}

###############################################################################

# Set draw date to the calendar date passed as the first argument to the script.
# If a calendar date is present, set the draw date. Otherwise, display usage
# information on the console.
if [[ ! -z ${1} ]]
then
  draw_date=${1}
else
  echo "Usage:    alc.sh YYYY-MM-DD"
  echo "Example:  alc.sh 2018-10-03"
  exit 1
fi

retrieve_lottery_results
extract_lottery_details
write_details_to_console
write_details_to_file
clean_up

exit 0
