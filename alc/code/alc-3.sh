#!/usr/bin/env sh

# Lottery Data Retrieval and Extraction for Atlantic Lottery Corporation
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Original source:
#   https://gitlab.com/gregorydhorne/alc-lotto-649-data-retrieval 
# Copyright (c) 2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

# Configuration Settings
BROWSER="wget -O ./data/page.html"
PROTOCOL="https"
DOMAIN="www.alc.ca"

# Validate the lottery game designator. 
validate()
{
  # If a lottery name has not been passed as the first argument, display usage
  # message.
  if [[ "${1}" != "lotto-6-49" ]]
  then
    echo "Usage:    alc.sh lottery"
    echo "Example:  alc.sh lotto-6-49"
    exit 1
  fi
}

# Retrieve results for the specified draw dates contained in the file
# named draw-dates.
lotto_game()
{
  local game=${1}

  local error_count=0
  local file_name=$(echo ${game} | sed 's/-//g')
  local today=$(date -I | sed 's/-//g')

  local draw_date
  local line

  while IFS='' read -r line || [[ -n "${line}" ]]
  do
    draw_date=$(echo ${line} | sed 's/-//g')
    if [[ "${draw_date}" -lt "${today}" ]]
    then
      draw_date=${line}
      error_count=$(retrieve_lottery_results ${game} ${draw_date} ${error_count})
      if [[ -e ./data/page.html ]]
      then
        results=$(extract_lottery_details ${game} ${draw_date})
        write_details_to_file "${results}"
      fi
      clean_up
    fi
  done < ./data/draw-dates
  
  temp_file=$(mktemp)
  sort ./data/${file_name}.csv > ${temp_file}
  cp ${temp_file} ./data/${file_name}.csv

  echo ${error_count}
}

# Retrieve lottery results for the specified draw date.
retrieve_lottery_results()
{
  local lottery=${1}
  local draw_date=${2}
  local error_count=${3}

  local file_name=$(echo ${lottery} | sed 's/-//g')

  # If the results for the draw date have already been retrieved, do not fetch
  # them again.
  if [[ ! -e ./data/${file_name}.csv ]] || [[ -z $(grep "${draw_date}" ./data/${file_name}.csv) ]]
  then
    # Launch the web browser and retrieve the Lotto 649 winning numbers for the
    # specified draw date. The date is not validated to ensure it is an actual
    # draw date; if the date is incorrect, the lottery results returned are for
    # the draw date prior to the calendar date.
    ASSET="/content/alc/en/our-games/lotto/${lottery}.html"
    QUERY="?date=${draw_date}"
    ${BROWSER} ${PROTOCOL}://${DOMAIN}${ASSET}${QUERY}

    # Manually save the web page to a file named page.html.
    if [[ ! -e ./data/page.html ]]
    then
      error_count=$(expr ${error_count} + 1)
    fi
  fi

  echo ${error_count}
}

# Extract the JSON formatted data about the lottery from the HTML file.
# Create a well-formed JSON data structure and extract lottery details (gameId
# and gameData).
extract_lottery_details()
{
  local lottery=${1}
  local draw_date=${2}

  local file_name=$(echo ${lottery} | sed 's/-//g')

  local bonus_number
  local jackpot
  local numbers

  awk '
    /ALC.components.GameDetailComponent/ { flag = 1; next; }
    /jQuery/ { flag = 0; } flag
  ' ./data/page.html \
  | awk '
      BEGIN { print("["); }
      /gameId/ {
        sub("gameId", "\"gameId\"", $0);
        printf("{%s", $0);
      }
      /gameData/ {
        sub("gameData", "\"gameData\"", $0);
        sub(/,$/, "},", $0);
        printf("%s\n", $0);
      }
      END { printf("{}\n]"); }
  ' > ./data/page.json

  # Extract the winning numbers and the bonus number.
  numbers=$(jq --raw-output '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.winning_numbers | @csv' ./data/page.json |  sed 's/\"//g')
  bonus_number=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.bonus_number' ./data/page.json | sed 's/\"//g')

  # Extract the jackpot amount.
  jackpot=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.prize_payouts | .[] | select(.type=="Lotto649_6of6") | .prize_value' ./data/page.json)
  jackpot=$(printf "%0.2f" ${jackpot})

  echo "${lottery} ${draw_date},${numbers},${bonus_number},${jackpot}"
}

# Save the lottery results to a file named lotto649.csv.
write_details_to_file()
{
  local file_name=$(echo ${1} | cut -d \  -f 1 | sed 's/-//g')
  local results=$(echo ${1} | cut -d \  -f 2)

  printf "${results}\n" >> ./data/${file_name}.csv
}

# Get the current record count, that is the number of lottery draws stored in
# the lottery details file.
record_count()
{
  local file_name=$(echo ${1} | sed 's/-//g')

  if [[ -e ./data/${file_name}.csv ]]
  then
    echo $(wc -l ./data/${file_name}.csv | cut -d \  -f 1)
  else
    echo 0
  fi
}

# Display the number of lottery draws stored in the lottery details file.
status()
{
  local pre_update_count=${1}
  local post_update_count=${2}
  local error_count=${3}

  printf "Status:"
  printf "\tRecord count: ${pre_update_count} (pre-count) : "
  printf "${post_update_count} (post-count)\n"
  printf "\tError count: ${error_count}\n"
}

# Clean-up any intermediary files.
clean_up()
{
  rm -f ./data/page.html ./data/page.json
}

###############################################################################

validate ${1}
pre_update_count=$(record_count ${1})
error_count=$(lotto_game ${1})
post_update_count=$(record_count ${1})
status ${pre_update_count} ${post_update_count} ${error_count}

exit 0
