# Lottery Data Retrieval and Extraction for Atlantic Lottery Corporation
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Original source:
#   https://gitlab.com/gregorydhorne/alc-lotto-649-data-retrieval
# Copyright (c) 2017-2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

/ALC.components.GameDetailComponent/ { flag = 1; next; }
  /jQuery/ { flag = 0; } flag
' page.html \
| awk '
    BEGIN { print("["); }
    /gameId/ {
      sub("gameId", "\"gameId\"", $0);
      printf("{%s", $0);
    }
    /gameData/ {
      sub("gameData", "\"gameData\"", $0);
      sub(/,$/, "},", $0);
      printf("%s\n", $0);
    }
    END {
      printf("{}\n]");
    }
