# A Data Retrieval and Extraction Tool for Atlantic Lottery Corporation's Lotto 649

A
<a href="https://www.gregoryhorne.ca/posts/introduction-to-iterative-development/" target="_blank">case study</a>
detailing the creation of a data retrieval and extraction
programme, applying the iterative development methodology, to collect
historical lottery data for a lottery game operated by Atlantic Lottery
Corporation provides further information.

Beginning with a minimum viable programme to retrieve historic lottery data the
article details a succession of code reorganisations and feature improvements
leading to a fully automated, production-ready data retrieval &amp; programme
suitable for deployment in a scheduled or batch processing environment.

The data retrieval programmes expect an environment as defined in the
<a href="https://gitlab.com/gregorydhorne/alc-lotto-649-data-retrieval/blob/master/Dockerfile" target="_blank">Dockerfile</a>
.

- <a href="https://alpinelinux.org" target="_blank">Alpine Linux</a>
  - ash, awk, cal, sed, unzip, wget, zip
- <a href="https://stedolan.github.io/jq/" target="_blank">jq parser</a>

## Usage

| Task                  | Invocation                                                                     |
| --------------------- | -------------------------------------------------------------------------------|
| Build the container   | docker build --tag alc .                                                       |
| Retrieve lottery data |                                                                                |
| (interactive mode)    | $ docker run --rm  --volume ${PWD}/alc:/home/user/project alc                  |
|                       | $ ./code/alc-[0-4].sh                                                          |
| (batch mode)          | $ docker run --rm -it -v ${PWD}/alc:/home/user/project alc ./code/alc-[0-4].sh |

The dataset is stored in the ./data directory which maps to local storage ${PWD}/alc/data.

## Examples (interactive mode)

```
$ ./code/alc-0.sh
Usage:    alc.sh YYYY-MM-DD
Example:  alc.sh 2018-10-03
$
$ ./code/alc-0.sh 2018-10-03
Lottery results for 2018-10-03: 15,32,38,40,44,45,02,40000000
$
$ ./code/alc-0.sh 2018-10-03
Lottery results for 2018-10-03 were previously retrieved
2018-10-03,15,32,38,40,44,45,02,40000000
```
```
$ ./code/alc-1.sh 
Usage:    alc.sh YYYY-MM-DD
Example:  alc.sh 2018-10-03
$
$ ./code/alc-1.sh 2018-10-03
Lottery results for 2018-10-03: 15,32,38,40,44,45,02,40000000
$
$ ./code/alc-1.sh 2018-10-03
Lottery results for 2018-10-03 were previously retrieved
2018-10-03,15,32,38,40,44,45,02,40000000
```
```
$ ./code/alc-2.sh 
Usage:    alc.sh YYYY-MM-DD
Example:  alc.sh 2018-10-03
$
$ ./code/alc-2.sh 20181003
Usage:    alc.sh YYYY-MM-DD
Example:  alc.sh 2018-10-03
$
$ ./code/alc-2.sh 2018-10-03
Lottery results for 2018-10-03: 15,32,38,40,44,45,02,40000000
$
$ ./code/alc-2.sh 2018-10-03
Lottery results for 2018-10-03 were previously retrieved
2018-10-03,15,32,38,40,44,45,02,40000000
```
```
$ ./code/alc-3.sh 
Usage:    alc.sh lottery
Example:  alc.sh lotto-6-49
$
$ ./code/alc-3.sh lotto-6-49
Status: Record count: 0 (pre-count) : 2 (post-count)
        Error count: 0
$
$ ./code/alc-3.sh lotto-6-49
Status: Record count: 2 (pre-count) : 2 (post-count)
        Error count: 0
```
```
$ ./code/alc-4.sh lotto-6-49 2018 2018
Data retrieval starting...with errors
Status:
  Record count: 0 (pre-count) : 103 (post-count)
  Error count: 1
  Archive created
$ ./code/alc-4.sh lotto-6-49 2018 2018
Data retrieval starting...completed successfully
  Archive updated
$ ./code/alc-4.sh lotto-6-49 2018 2018
Data retrieval starting...completed successfully
  No updates to archive
$ ./code/alc-4.sh lotto-6-49 2018 2019
Data retrieval starting...completed successfully
  Archive updated
```

## Examples (non-interactive batch mode)

```
$ docker run --rm -it -v ${PWD}/alc:/home/user/project alc ./code/alc-4.sh lotto-6-49 2017 2019
Data retrieval starting...completed successfully
	Archive updated
```

## License

BSD 3-Clause License
